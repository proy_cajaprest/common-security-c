package com.ebiz.common.security.resource;

import java.util.List;
import com.ebiz.common.security.User;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Principal {
	
	@JsonProperty("password")
	private String password;
	
	@JsonProperty("username")
	private String username;
	
	@JsonProperty("authorities")
	private List<Authority> authorities;
	
	@JsonProperty("user")
	private User User;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<Authority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(List<Authority> authorities) {
		this.authorities = authorities;
	}

	public User getUser() {
		return User;
	}

	public void setUser(User user) {
		User = user;
	}
	
	
	
	

}
