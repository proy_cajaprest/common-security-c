package com.ebiz.common.security.resource;


import java.util.List;

import com.ebiz.common.security.User;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by manuelzegarra on 1/12/17.
 */

public class OAuthResponse {
	
	
	@JsonProperty("details")
	private Details details;
	
	@JsonProperty("authorities")
	private List<Authority> authorities;
	
	@JsonProperty("authenticated")
	private boolean authenticated;
	
	@JsonProperty("oauth2Request")
	private OAuth2Request oauth2Request;
	
	

    @JsonProperty("user")
    private User user;
    
    @JsonProperty("principal")
    private Principal principal;
    
    @JsonProperty("name")
    private String name;
    
    
    
    
	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public Principal getPrincipal() {
		return principal;
	}



	public void setPrincipal(Principal principal) {
		this.principal = principal;
	}



	public Details getDetails() {
		return details;
	}



	public void setDetails(Details details) {
		this.details = details;
	}



	public List<Authority> getAuthorities() {
		return authorities;
	}



	public void setAuthorities(List<Authority> authorities) {
		this.authorities = authorities;
	}



	public boolean isAuthenticated() {
		return authenticated;
	}



	public void setAuthenticated(boolean authenticated) {
		this.authenticated = authenticated;
	}



	public OAuth2Request getOauth2Request() {
		return oauth2Request;
	}



	public void setOauth2Request(OAuth2Request oauth2Request) {
		this.oauth2Request = oauth2Request;
	}



	public User getUser() {
		return user;
	}



	public void setUser(User user) {
		this.user = user;
	}



}
