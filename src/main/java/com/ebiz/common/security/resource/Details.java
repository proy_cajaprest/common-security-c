package com.ebiz.common.security.resource;


import com.fasterxml.jackson.annotation.JsonProperty;

public class Details {
		
	@JsonProperty("remoteAddress")
	private String remoteAddress;
	
	@JsonProperty("sessionId")
	private String sessionId;
	
	@JsonProperty("tokenValue")
	private String tokenValue;
	
	@JsonProperty("decodedDetails")
	private String decodedDetails;

	public String getRemoteAddress() {
		return remoteAddress;
	}

	public void setRemoteAddress(String remoteAddress) {
		this.remoteAddress = remoteAddress;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getTokenValue() {
		return tokenValue;
	}

	public void setTokenValue(String tokenValue) {
		this.tokenValue = tokenValue;
	}

	public String getDecodedDetails() {
		return decodedDetails;
	}

	public void setDecodedDetails(String decodedDetails) {
		this.decodedDetails = decodedDetails;
	}

}
