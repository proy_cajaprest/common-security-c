package com.ebiz.common.security.resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

import com.ebiz.common.security.User;


public class PrincipalUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PrincipalUtil.class);
	
	public static User getUser(){
		
		/*LOGGER.info("getUser:1");
		
		OAuth2Authentication oAuth2Authentication = (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();
		
		LOGGER.info("oAuth2Authentication:{}",oAuth2Authentication);
		
        Authentication authentication = oAuth2Authentication.getUserAuthentication();
        
        LOGGER.info("authentication:{}",authentication);
        
        User user =  (User)authentication.gt
        
        return user;*/
		
			
		OAuth2Authentication oAuth2Authentication = (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();
        Authentication authentication = oAuth2Authentication.getUserAuthentication();
        User user =  (User)authentication.getDetails();
        
        return user;
		
                
	}

}
