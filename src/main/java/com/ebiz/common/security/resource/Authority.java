package com.ebiz.common.security.resource;


import com.fasterxml.jackson.annotation.JsonProperty;

public class Authority {
	
	
	@JsonProperty("authority")
	private String authority;

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}
	
	

}
