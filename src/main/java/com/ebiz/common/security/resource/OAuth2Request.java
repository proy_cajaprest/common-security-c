package com.ebiz.common.security.resource;


import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OAuth2Request {
	
	
	@JsonProperty("clientId")
	private String clientId;
	
	@JsonProperty("scope")
	private List<String> scope;

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public List<String> getScope() {
		return scope;
	}

	public void setScope(List<String> scope) {
		this.scope = scope;
	}
	
	

}
