package com.ebiz.common.security;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class User implements Serializable {

	private static final long serialVersionUID = 6128016096756071380L;

	public User() {
		if (organizaciones == null) {
			organizaciones = new ArrayList<>();
		}
		if (opciones == null) {
			opciones = new ArrayList<>();
		}
	}

	private List<Organizacion> organizaciones;

	private List<Opcion> opciones;
	
	private UUID id;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String direccion;
	private String telefono;
	private String anexo;
	private String email;

	private String tipoDocumento;
	private String numeroDocumento;

	private String usuario;
	private String password;
	private String campo1;
	private String campo2;
	private String campo3;
	private String campo4;
	private String campo5;

	private short habilitado;
	private String contacto;
	private String titulo;
	private String fax;
	private double montoMaximo;
	private short habilitadoEnvioMails;
	private short porAprobar;
	private String idioma;
	private short boletin;

	private String monedaTablaId;
	private String monedaRegistroId;

	private String paisTablaId;
	private String paisRegistroId;
	
	private String avatar;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getAnexo() {
		return anexo;
	}

	public void setAnexo(String anexo) {
		this.anexo = anexo;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public short getHabilitado() {
		return habilitado;
	}

	public void setHabilitado(short habilitado) {
		this.habilitado = habilitado;
	}

	public String getContacto() {
		return contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public List<Organizacion> getOrganizaciones() {
		return organizaciones;
	}

	public void setOrganizaciones(List<Organizacion> organizaciones) {
		this.organizaciones = organizaciones;
	}

	public List<Opcion> getOpciones() {
		return opciones;
	}

	public void setOpciones(List<Opcion> opciones) {
		this.opciones = opciones;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCampo1() {
		return campo1;
	}

	public void setCampo1(String campo1) {
		this.campo1 = campo1;
	}

	public String getCampo2() {
		return campo2;
	}

	public void setCampo2(String campo2) {
		this.campo2 = campo2;
	}

	public String getCampo3() {
		return campo3;
	}

	public void setCampo3(String campo3) {
		this.campo3 = campo3;
	}

	public String getCampo4() {
		return campo4;
	}

	public void setCampo4(String campo4) {
		this.campo4 = campo4;
	}

	public String getCampo5() {
		return campo5;
	}

	public void setCampo5(String campo5) {
		this.campo5 = campo5;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public double getMontoMaximo() {
		return montoMaximo;
	}

	public void setMontoMaximo(double montoMaximo) {
		this.montoMaximo = montoMaximo;
	}

	public short getHabilitadoEnvioMails() {
		return habilitadoEnvioMails;
	}

	public void setHabilitadoEnvioMails(short habilitadoEnvioMails) {
		this.habilitadoEnvioMails = habilitadoEnvioMails;
	}

	public short getPorAprobar() {
		return porAprobar;
	}

	public void setPorAprobar(short porAprobar) {
		this.porAprobar = porAprobar;
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	public short getBoletin() {
		return boletin;
	}

	public void setBoletin(short boletin) {
		this.boletin = boletin;
	}

	public String getMonedaTablaId() {
		return monedaTablaId;
	}

	public void setMonedaTablaId(String monedaTablaId) {
		this.monedaTablaId = monedaTablaId;
	}

	public String getMonedaRegistroId() {
		return monedaRegistroId;
	}

	public void setMonedaRegistroId(String monedaRegistroId) {
		this.monedaRegistroId = monedaRegistroId;
	}

	public String getPaisTablaId() {
		return paisTablaId;
	}

	public void setPaisTablaId(String paisTablaId) {
		this.paisTablaId = paisTablaId;
	}

	public String getPaisRegistroId() {
		return paisRegistroId;
	}

	public void setPaisRegistroId(String paisRegistroId) {
		this.paisRegistroId = paisRegistroId;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
}
