package com.ebiz.common.security;

import java.io.Serializable;
import java.util.UUID;

public class Organizacion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private UUID id;
	private String nombre;
	private String tipoEmpresa;
	private String keySuscripcion;
	private String ruc;
	private String isoPais;
	private String logo;
	
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTipoEmpresa() {
		return tipoEmpresa;
	}
	public void setTipoEmpresa(String tipoEmpresa) {
		this.tipoEmpresa = tipoEmpresa;
	}
	public String getKeySuscripcion() {
		return keySuscripcion;
	}
	public void setKeySuscripcion(String keySuscripcion) {
		this.keySuscripcion = keySuscripcion;
	}
	
	public String getRuc() {
		return ruc;
	}
	public void setRuc(String ruc) {
		this.ruc = ruc;
	}
	public String getIsoPais() {
		return isoPais;
	}
	public void setIsoPais(String isoPais) {
		this.isoPais = isoPais;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	
	
}
