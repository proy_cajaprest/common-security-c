package com.ebiz.common.security.web.filter;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;

import com.ebiz.common.security.Organizacion;
import com.ebiz.common.security.User;
import com.ebiz.common.security.resource.PrincipalUtil;

public class SecurityFilter implements Filter {

	protected final Log logger = LogFactory.getLog(getClass());

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		User user = PrincipalUtil.getUser();
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		
		
		if (user != null) {
			Map<String, String> headers = getHeadersInfo(httpRequest);
			String organizationId = headers.get("org_id");
			
			if(organizationId==null) {
				
				logger.debug("Debe definir el párametro org_id");
								
			

			}else{
				
				logger.debug("User:" + user.getUsuario());
				logger.debug("OrgId:" + organizationId);
				logger.debug("Cantidad Organizaciones:" + String.valueOf(user.getOrganizaciones().size()));
				
				Organizacion organizacion = user.getOrganizaciones().stream().filter(p -> p.getId().toString().compareTo(organizationId)==0)
						.findAny()
						.orElse(null);

				if(organizacion==null) {
					logger.debug("No autorizado");
					httpResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
				}else {
			        chain.doFilter(request, response); 
				}
			}
			
		} else {
			logger.debug("User nullo");
		}

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	private Map<String, String> getHeadersInfo(HttpServletRequest request) {

		Map<String, String> map = new HashMap<String, String>();

		Enumeration<String> headerNames = request.getHeaderNames();
		
		while (headerNames.hasMoreElements()) {
			String key = (String) headerNames.nextElement();
			String value = request.getHeader(key);
			
			
			map.put(key, value);
		}

		return map;
	}

}
