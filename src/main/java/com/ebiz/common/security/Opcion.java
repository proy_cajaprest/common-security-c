package com.ebiz.common.security;

import java.io.Serializable;
import java.util.UUID;
//forzando ci
public class Opcion implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public String in_idopcion;
    public String in_idpantalla;
    public String vc_descripcion;

    public String getIn_idopcion() {
        return in_idopcion;
    }

    public void setIn_idopcion(String in_idopcion) {
        this.in_idopcion = in_idopcion;
    }

    public String getIn_idpantalla() {
        return in_idpantalla;
    }

    public void setIn_idpantalla(String in_idpantalla) {
        this.in_idpantalla = in_idpantalla;
    }

    public String getVc_descripcion() {
        return vc_descripcion;
    }

    public void setVc_descripcion(String vc_descripcion) {
        this.vc_descripcion = vc_descripcion;
    }

    
    

}